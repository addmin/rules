
**[1. Reasoning Engine](#heading--1)**

This repository contains:
- **SHACL Validation Rules**
- **SHACL Inferences Rules for documents tagging**
- **SHACL Inferences Rules for profiling**
---

**[1.1. How to do](#heading--1-1)**

**[1.1.1 Install TopBraid Composer Maestro Edition version 7.1.1](#heading--1-1-1)**

TopBraid Composer Maestro Edition version 7.1.1 which is developed by TopQuadrant, Inc. TopBraid is a visual modelling environment from industry experts for creating and managing domain models and ontologies in the Semantic Web standards RDF, RDFS and OWL.

Click on this link [TopBraid](https://www.topquadrant.com/topbraid-composer-install/) to install the software.


**[1.1.2 Model SHACL Constraints](#heading--1-1-2)**

- Import the [Ontology](https://gitlab.unige.ch/addmin/doc-onto/-/blob/main/doc-onto.owl.ttl) into TopBraid. 

- Create the <abbr title="Shapes Constraint Language">SHACL </abbr> node shape for each interested class of the ontology. Each node shape is connected to the corresponding class through <abbr title="Shapes Constraint Language">SHACL </abbr> `sh:targetClass`. A target class allows that the definition of a node shape applies to all instances of a class. For example, **A3èmePilierACotisationShape** is a type of node shape, and it connected to the **A3èmePilierACotisationClass**.

- Add the relevant features as part of a node shape. We point out the relevant features as property shapes using the `sh:property` predicate. For example, some relevant features for **A3èmePilierACotisationShape** are: *NomPersonne* and *noAVS*.

- <abbr title="Shapes Constraint Language">SHACL </abbr> is an official <abbr title="World Wide Web Consortium">W3C</abbr> standard language, which allows describing a set of conditions data.


**[1.2. Rules](#heading--1-2)**

**[1.2.1 SHACL Validation Rules](#heading--1-2-1)**

- SHACL validation allows us to validate that data are conforming to desired requirements. The result describes whether the data graph conforms to the shapes graph and, if it does not, describes each non-compliance.

- Create a PersonneValidationTestCase writing data within the [PersonneShape.test.ttl file](https://gitlab.unige.ch/addmin/rules/-/blob/main/test/PersonneShape.test.ttl).  

- Set up two invalid resources that do not meet the shape requirements (concerning a person) and one valid resource that meets the required requirements. For example, we code two invalid *noAVS* which do not match the proper pattern of *noAVS*.

- Import [PersonneShape.test.ttl file](https://gitlab.unige.ch/addmin/rules/-/blob/main/test/PersonneShape.test.ttl) into TopBraid.

- Add data (created through [RML Mapping](https://gitlab.unige.ch/addmin/rml-mappings)) into TopBraid.

- Run `Test Cases` on TopBraid to see the results of the validation test.

There are six [Examples of Validation Test Cases](https://gitlab.unige.ch/addmin/rules/-/tree/main/test).


**[1.2.2 SHACL Inferences Rules for documents tagging](#heading--1-2-2)**

- Model [<abbr title="Shapes Constraint Language">SHACL </abbr> rules for making a multi-label classification](https://gitlab.unige.ch/addmin/rules/-/blob/main/addmin.shapes.ttl) with regard to each document that is a class.

- Use `tag` to describe the label category for each document which represents a class. 

- Assign many `tags` to each document to make a multi-label classification. For example, the **Health Insurance** falls into two categories: *Insurance* and *Tax*. 

- Add data (created through [RML Mapping](https://gitlab.unige.ch/addmin/rml-mappings)) into TopBraid.

- Click on `Run Inferences` to see outcomes.


**[1.2.3 SHACL Inferences Rules for profiling](#heading--1-2-3)**

- Model <abbr title="Shapes Constraint Language">SHACL </abbr> rules for making [direct rules](https://gitlab.unige.ch/addmin/rules/-/blob/main/addmin.shapes.ttl) and [inverse rules](https://gitlab.unige.ch/addmin/rules/-/blob/main/addmin.shapes.ttl) between users profile and documents. For example:

      > A wage-earning worker has to deliver a Salary Certificate (=Direct Relationship)

      > If someone delivers a Salary Certificate, he/she is a wage-earner worker (=Reverse Relationship)

- Add data (created through [RML Mapping](https://gitlab.unige.ch/addmin/rml-mappings)) into TopBraid.

- Click on `Run Inferences` to see outcomes.

